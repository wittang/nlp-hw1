Wilson Tang
107513801
CSE390

Necessary Files:
lmBuilder.py
bigram-query.py
perplexity.py
train.txt

To begin: Open terminal and navigate to location of files

lmBuilder.py
To run the Language Model Builder, in the terminal, enter: python3 lmBuilder.py train.txt
where, train.txt is the training set
This program will create the following three files: unigram.lm, bigram.lm, and top-bigrams.txt

bigram-query.py
To run the Bigram Query program, in the terminal, enter: python3 bigram-query.py bigram.lm unigram.lm <x> <y> <s>
where, bigram.lm and unigram.lm are output files from running lmBuilder.py
and 
<x> is the first word, <y> is the second word to find bigram probabilities depending on the smoothing, <s>, selection specfied by the user.
values for s are: M, L, I, and K

perplexity.py
To run the Perplexity program, in the terminal, enter: python3 perplexity.py bigram.lm unigram.lm test.txt
where, bigram.lm and unigram.lm are output files from running lmBuilder.py
and
test.txt is the file used to find perplexities of the language models