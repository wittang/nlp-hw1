#Wilson Tang
#107513801
#CSE390

import sys
import math
import string

#Global variables - unigram dictionary, bigram dictionary and lambda value
unigram_Count = {}
bigram_Count = {}
l_lambda = 0.1

#Preprocess Open File. Lowercase all text. Split on '.'. Add start (<s>) and end (</s>) symbols to each sentence. 
def preprocess(file):
	training_File = open(file)
	training_String = training_File.read().lower()
	training_Data = training_String.split('.')
	for index in range(len(training_Data)):
		training_Data[index] = "<s> " + training_Data[index] + " </s>"
		##Remove Punctuation
		#training_Data[index] = "<s>" + training_Data[index].translate(None, string.punctuation) + " </s>"
	return training_Data

#If word exists, increase count. Otherwise, add word to dictionary. 
def dict_update(dict, key):
	if key in dict:
		dict[key] +=1
	else:
		dict[key] = 1

#Count words for unigram and bigrams
def count_Words(training_Data):
	for sentence in training_Data:
		words = sentence.split()
		for index in range(len(words)):
			if index < len(words) - 1:
				dict_update(unigram_Count, words[index])
				bigram_Words = words[index] + " " + words[index + 1]
				dict_update(bigram_Count, bigram_Words)
			else:
				dict_update(unigram_Count, words[index])
				bigram_Words = words[index] + " <s>"
				dict_update(bigram_Count, bigram_Words)

#Return total words
def total_tkns():
	count = 0
	for word in unigram_Count:
		count += unigram_Count[word]
	return count

#Calculate MLE probability for given words xy 
def calc_MLE(xy):
	x = xy.split()[0]
	if xy in bigram_Count and x in unigram_Count:
		count_xy = bigram_Count[xy]
		count_x = unigram_Count[x]
		return float(count_xy)/float(count_x)
	else:
		return 0

#Calculate unigram Laplace probability for given word x
def calc_uni_L(x):
	count_x = 0
	if x in unigram_Count:
		count_x = unigram_Count[x]
	return float(count_x + 1)/float(total_tkns() + len(unigram_Count) + 1)

#Calculate bigram Laplace probability for given words xy
def calc_bi_L(xy):
	x = xy.split()[0]
	count_xy = 0
	count_x = 0
	if xy in bigram_Count:
		count_xy = bigram_Count[xy]
	if x in unigram_Count:
		count_x = unigram_Count[x]
	return float(count_xy + 1)/(count_x + len(unigram_Count) + 1)

#Calculate bigram Interpolation probability for given words xy
def calc_INT(xy):
	y = xy.split()[1]
	return (l_lambda * calc_MLE(xy)) + (1 - l_lambda) * (calc_uni_L(y))

#Calculate Katz Backoff probability for given words xy
def calc_K(xy):
	x = xy.split()[0]
	count_xy = bigram_Count[xy]
	count_x = unigram_Count[x]
	return float(count_xy - 0.5)/float(count_x)

#Calculate joint probability for given words xy
def calc_JointP(xy):
	x = xy.split()[0]
	return calc_uni_L(x) * calc_bi_L(xy)

#Calculate perplexity for dev.txt to determine lambda value to be used for INT probability
def calc_perplexity():
	dev_File = open("dev.txt")
	dev_String = "<s> " + dev_File.read().replace(".", " <s> </s>")
	dev_Words = dev_String.split()
	num_Words = len(dev_Words)
	sum_Prob = 0
	for index in range(len(dev_Words)):
		if index < len(dev_Words)-1:
			xy = dev_Words[index] + " " + dev_Words[index+1]
			sum_Prob += math.log(calc_INT(xy), 2)
	print(2**((-1/num_Words) * sum_Prob))

#Create bigram string to be written into bigram.lm file
def bigram_toString(words):
	mle_prob = calc_MLE(words)
	l_prob = calc_bi_L(words)
	int_prob = calc_INT(words)
	k_prob	= calc_K(words)
	return words.split()[0] + '\t' + words.split()[1] + '\t' + str(bigram_Count[words]) + '\t' + str(mle_prob) + '\t' + str(l_prob) + '\t' + str(int_prob) + '\t' + str(k_prob) + '\n'
	
#Main function - preprocess train.txt. Count Words. Create unigram file and write counts. Create bigram file and write bigram strings. 
#Create top-bigrams file and write top 20 bigrams found based on joint probabilities calculated. 
def main():
	training_Data = preprocess(sys.argv[1])
	count_Words(training_Data)

	unigram_File = open("unigram.lm", "w")
	for word in unigram_Count:
		unigram_File.write(word + '\t' + str(unigram_Count[word]) + '\n')
	unigram_File.close()

	bigram_File = open("bigram.lm", "w")
	top_Bigrams = open("top-bigrams.txt", "w")
	bigram_Array = []
	for words in bigram_Count:
		if words.split()[0] != "<s>" and words.split()[0] != "</s>" and words.split()[1] != "<s>" and words.split()[1] != "</s>": 
			bigram_Array.append([words, calc_JointP(words)])
		bigram_File.write(bigram_toString(words))
	bigram_File.close()

	bigram_Array_Sorted = sorted(bigram_Array, key=lambda x: x[1], reverse=True)[0:20]
	for bigram_Data in bigram_Array_Sorted:
		top_Bigrams.write(bigram_Data[0] + '\t' + str(bigram_Data[1]) + '\n')
	#print(total_tkns())
	#calc_perplexity()

main()


