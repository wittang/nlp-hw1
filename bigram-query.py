#Wilson Tang
#107513801
#CSE390

import sys
import re

#Global variables - unigram dictionary, bigram dictionary and lambda value
unigram_Count = {}
bigram_Count = {}
l_lambda = 0.1

#Read unigram.lm file and fill in unigram dictionary
def unigram_Setup(file):
	unigram_Rows = open(file).read().splitlines()
	for row in unigram_Rows:
		row_Data = re.split(r'\t+', row)
		unigram_Count[row_Data[0]] = int(row_Data[1])

#Read bigram.lm file and fill in bigram dictionary
def bigram_Setup(file):
	bigram_Rows = open(file).read().splitlines()
	for row in bigram_Rows:
		row_Data = re.split(r'\t+', row)
		bigram_Count[row_Data[0] + " " + row_Data[1]] = int(row_Data[2])

#Return total words in file
def total_tkns():
	count = 0
	for word in unigram_Count:
		count += unigram_Count[word]
	return count

#Calculate MLE probability for given words xy 
def calc_MLE(xy):
	x = xy.split()[0]
	if xy in bigram_Count and x in unigram_Count:
		count_xy = bigram_Count[xy]
		count_x = unigram_Count[x]
		return float(count_xy)/float(count_x)
	else:
		return 0

#Calculate unigram Laplace probability for given words xy 
def calc_uni_L(x):
	count_x = 0
	if x in unigram_Count:
		count_x = unigram_Count[x]
	return float(count_x + 1)/float(total_tkns() + len(unigram_Count) + 1)

#Calculate bigram Laplace probability for given words xy 
def calc_bi_L(xy):
	x = xy.split()[0]
	count_xy = 0
	count_x = 0
	if xy in bigram_Count:
		count_xy = bigram_Count[xy]
	if x in unigram_Count:
		count_x = unigram_Count[x]
	return float(count_xy + 1)/(count_x + len(unigram_Count) + 1)

#Calculate bigram Interpolation probability for given words xy 
def calc_INT(xy):
	y = xy.split()[1]
	return (l_lambda * calc_MLE(xy)) + (1 - l_lambda) * (calc_uni_L(y))

#Find probability in bigram.lm based on smoothing selection specified by user
def bigram_Prob(file, x, y, smoothing):
	pr_String = "Pr(" + y + "|" + x + ") = "
	bigram_Rows = open(file).read().splitlines()
	for row in bigram_Rows:
		row_Data = re.split(r'\t+', row)
		if row_Data[0] == x and row_Data[1] == y:
			if smoothing == 'M':
				return pr_String + row_Data[3]
			elif smoothing == 'L':
				return pr_String + row_Data[4]
			elif smoothing == 'I':
				return pr_String + row_Data[5]
			elif smoothing == 'K':
				return pr_String + row_Data[6]
			else:
				return ""

#Find sum of unigram Laplace for words following x and sum of unigram Laplace for words not following x. 
#Required to find alpha and beta for Katz backoff when xy is not found in training set
def calc_Kvalues(x):
	list_w = []
	sum_xw_prob = 0
	sum_w_prob = 0
	bigram_Rows = open(sys.argv[1]).read().splitlines()
	for row in bigram_Rows:
		row_Data = re.split(r'\t+', row)
		if row_Data[0] == x:
			sum_xw_prob += float(row_Data[6])
			list_w.append(row_Data[1])
	for word in unigram_Count:
		if word not in list_w:
			sum_w_prob += calc_uni_L(word)
	return sum_xw_prob, sum_w_prob

#If bigram is not found in training set, calculate probability based on smoothing selection specified by user
def find_Prob(x, y, smoothing):
	xy = x + " " + y
	pr_String = "Pr(" + y + "|" + x + ") = "
	if smoothing == 'M':
		return pr_String + "0" 
	elif smoothing == 'L':
		return pr_String + str(calc_bi_L(xy))
	elif smoothing == 'I':
		return pr_String + str(calc_INT(xy))
	elif smoothing == 'K':
		sum_xw_prob, sum_w_prob = calc_Kvalues(x)
		return pr_String + str((1 - sum_xw_prob)*(float(calc_uni_L(y))/sum_w_prob))
	else: 
		return "Invalid Smoothing Selection"

#Main function - run functions to find probabilty of bigram based on smoothing selection. bigram and smoothing selection specified by user
def main():
	x = sys.argv[3]
	y = sys.argv[4]
	smoothing = sys.argv[5].upper()
	result = bigram_Prob(sys.argv[1], x, y, smoothing)
	if not result:
		unigram_Setup(sys.argv[2])
		bigram_Setup(sys.argv[1])
		result = find_Prob(x, y, smoothing)
	print(result)

main()

