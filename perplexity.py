#Wilson Tang
#107513801
#CSE390

#Global variables - unigram dictionary, bigram dictionary and lambda value
import sys
import re
import math

unigram_Count = {}
bigram_Count = {}
l_lambda = 0.1

#Read unigram.lm file and fill in unigram dictionary
def unigram_Setup(file):
	unigram_Rows = open(file).read().splitlines()
	for row in unigram_Rows:
		row_Data = re.split(r'\t+', row)
		unigram_Count[row_Data[0]] = int(row_Data[1])

#Read bigram.lm file and fill in bigram dictionary
def bigram_Setup(file):
	bigram_Rows = open(file).read().splitlines()
	for row in bigram_Rows:
		row_Data = re.split(r'\t+', row)
		bigram_Count[row_Data[0] + " " + row_Data[1]] = int(row_Data[2])

#Preprocess file by lowering words and splitting sentences
def preprocess(file):
	test_File = open(file)
	test_String = "<s> " + test_File.read().lower().replace(".", " <s> </s>")
	return test_String.split()

#Return total words in file
def total_tkns():
	count = 0
	for word in unigram_Count:
		count += unigram_Count[word]
	return count

#Calculate MLE probability for given words xy 
def calc_MLE(xy):
	x = xy.split()[0]
	if xy in bigram_Count and x in unigram_Count:
		count_xy = bigram_Count[xy]
		count_x = unigram_Count[x]
		return float(count_xy)/float(count_x)
	else:
		return 0

#Calculate unigram Laplace probability for given words x
def calc_uni_L(x):
	count_x = 0
	if x in unigram_Count:
		count_x = unigram_Count[x]
	return float(count_x + 1)/float(total_tkns() + len(unigram_Count) + 1)

#Calculate bigram Laplace probability for given words xy 
def calc_bi_L(xy):
	x = xy.split()[0]
	count_xy = 0
	count_x = 0
	if xy in bigram_Count:
		count_xy = bigram_Count[xy]
	if x in unigram_Count:
		count_x = unigram_Count[x]
	return float(count_xy + 1)/(count_x + len(unigram_Count) + 1)

#Calculate bigram Intperolation probability for given words xy 
def calc_INT(xy):
	y = xy.split()[1]
	return (l_lambda * calc_MLE(xy)) + (1 - l_lambda) * (calc_uni_L(y))

#Calculate perplexity of test.txt 
def calc_Perplexities(file):
	test_Words = preprocess(file)
	num_Words = len(test_Words)
	sum_Prob_uni = 0
	sum_Prob_bi = 0
	sum_Prob_int = 0
	for index in range(len(test_Words)):
		if index < len(test_Words)-1:
			x = test_Words[index]
			xy = x + " " + test_Words[index+1]
			sum_Prob_uni += math.log(calc_uni_L(x), 2)
			sum_Prob_bi += math.log(calc_bi_L(xy), 2)
			sum_Prob_int += math.log(calc_INT(xy), 2)
		else:
			x = test_Words[index]
			sum_Prob_uni += math.log(calc_uni_L(x), 2)
	return 2**((-1/num_Words) * sum_Prob_uni), 2**((-1/num_Words) * sum_Prob_bi), 2**((-1/num_Words) * sum_Prob_int)

#Main function - Call functions to find perplexities based on the different models. 
def main(): 
	unigram_Setup(sys.argv[2])
	bigram_Setup(sys.argv[1])
	uni_Perplexity, bi_Perplexity, int_Perplexity = calc_Perplexities(sys.argv[3])
	print("\t\tPERPLEXITIES")
	print("Laplace Unigram Model:     " + str(uni_Perplexity) )
	print("Laplace Bigram Model:      " + str(bi_Perplexity)  )
	print("Interpolated Bigram Model: " + str(int_Perplexity) )

main()
